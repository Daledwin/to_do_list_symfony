<?php

namespace App\Tests\Unit\Validator;

use App\Entity\User;
use App\Validator\UserValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class UserValidatorTest extends TestCase
{

    public function testValidateAuthorReturnsTrueIfAuthorIsValid()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('Doe');
        $author->setEmail('john.doe@gmail.com');

        $result = $validator->validate($author);

        $this->assertTrue($result);
    }

    public function testValidateAuthorReturnsExceptionIfAuthorFirstNameIsBlank()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('');
        $author->setLastName('Doe');
        $author->setEmail('john.doe@gmail.com');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfFirstNameIsTooShort()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('J');
        $author->setLastName('Doe');
        $author->setEmail('john.doe@gmail.com');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfAuthorLastNameIsBlank()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('');
        $author->setEmail('john.doe@gmail.com');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfLastNameIsTooShort()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('D');
        $author->setEmail('john.doe@gmail.com');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfAuthorEmailIsBlank()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('Doe');
        $author->setEmail('');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfEmailIsTooShort()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('Doe');
        $author->setEmail('t@h.c');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }

    public function testValidateAuthorReturnsExceptionIfEmailIsNotAnEmail()
    {
        $validator = new UserValidator();
        $author = new User();
        $author->setFirstName('John');
        $author->setLastName('Doe');
        $author->setEmail('MonEmail');

        $this->expectException(InvalidParameterException::class);

        $validator->validate($author);
    }


}
