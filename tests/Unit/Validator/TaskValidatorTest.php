<?php

namespace App\Tests\Unit\Validator;

use App\Entity\Task;
use App\Validator\TaskValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class TaskValidatorTest extends TestCase
{
    public function testValidateTaskReturnsTrueIfTaskIsValid()
    {

        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('My task Title', $mockedAuthor, "My super content", new \DateTime('+1 week'), true);
        $result = $validator->validate($task);

        $this->assertTrue($result);
    }

    public function testValidateTaskReturnsExceptionIfTaskTitleIsBlank()
    {
        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('', $mockedAuthor, "My super content");

        $this->expectException(InvalidParameterException::class);

        $validator->validate($task);
    }

    public function testValidateTaskReturnsExceptionIfTaskTitleIsTooShort()
    {
        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('ToD', $mockedAuthor, "My super content");

        $this->expectException(InvalidParameterException::class);

        $validator->validate($task);
    }

    public function testValidateTaskReturnsExceptionIfTaskTitleIsTooLong()
    {
        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('To do something very long and complicated', $mockedAuthor, "My super content");

        $this->expectException(InvalidParameterException::class);

        $validator->validate($task);
    }

    public function testValidateTaskReturnsExceptionIfTaskDueDateIsInPast()
    {
        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('My task Title', $mockedAuthor, "My super content", new \DateTime('-1 day'));

        $this->expectException(InvalidParameterException::class);

        $validator->validate($task);
    }

    public function testValidateTaskReturnsExceptionIfTaskContentIsTooLong()
    {
        $validator = new TaskValidator();

        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task('My task Title', $mockedAuthor, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac neque volutpat, dictum est a, maximus sapien. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed varius justo non velit commodo, eget posuere sapien consequat. Sed maximus neque ac ipsum tincidunt, eget rhoncus libero dapibus. Duis ut ullamcorper magna. Nulla facilisi. Sed sed diam vel nulla luctus hendrerit. Aliquam congue commodo ex sit amet consectetur. Maecenas fermentum augue sed metus pulvinar euismod. Sed non fringilla nisl, ac cursus erat. Pellentesque nec lorem a libero accumsan venenatis. Suspendisse ultricies neque quis lectus bibendum, eget lobortis velit aliquet. Donec at nisi ut libero commodo viverra.");

        $this->expectException(InvalidParameterException::class);

        $validator->validate($task);
    }
}
