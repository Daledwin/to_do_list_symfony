<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use App\Entity\Task;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    public function testSetTitle()
    {
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task("TestTask", $mockedAuthor, "My super content");

        $title = 'My Task';
        $task->setTitle($title);
        $this->assertSame($title, $task->getTitle());
    }

    public function testSetDueDate()
    {
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task("TestTask", $mockedAuthor, "My super content");

        $dueDate = new \DateTime();
        $task->setDueDate($dueDate);
        $this->assertSame($dueDate, $task->getDueDate());
    }

    public function testSetStatus()
    {
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task("TestTask", $mockedAuthor, "My super content");

        $status = true;
        $task->setStatus($status);
        $this->assertSame($status, $task->getStatus());
    }

    public function testSetAuthor()
    {
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
        ->disableOriginalConstructor()
        ->getMock();

        $task = new Task("TestTask", $mockedAuthor, "My super content");


        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task->setAuthor($mockedAuthor);
        $this->assertSame($mockedAuthor, $task->getAuthor());
    }

    public function testSetContent()
    {
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $task = new Task("TestTask", $mockedAuthor, "My super content");

        $content = 'My beautiful Content';
        $task->setContent($content);
        $this->assertSame($content, $task->getContent());
    }
}
