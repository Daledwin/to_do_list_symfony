<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testSetFirstName()
    {
        $author = new User();
        $fistName = 'My firstName';
        $author->setFirstName($fistName);
        $this->assertSame($fistName, $author->getFirstName());
    }

    public function testSetLastName()
    {
        $author = new User();
        $lastName = 'My lastName';
        $author->setLastName($lastName);
        $this->assertSame($lastName, $author->getLastName());
    }

    public function testSetEmail()
    {
        $author = new User();
        $email = 'My email';
        $author->setEmail($email);
        $this->assertSame($email, $author->getEmail());
    }

    public function testSetRoles()
    {
        $author = new User();
        $email = 'My email';
        $author->setEmail($email);
        $this->assertSame($email, $author->getEmail());
    }

    public function testSetPassword()
    {
        $author = new User();
        $email = 'My email';
        $author->setEmail($email);
        $this->assertSame($email, $author->getEmail());
    }
}
