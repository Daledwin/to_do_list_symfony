<?php

namespace App\Tests\Unit\Service;

use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use App\Entity\Task;
use App\Service\TaskService;

class TaskServiceTest extends TestCase
{
    public function testCreateTask()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $mockedAuthor = $this->getMockBuilder('App\Entity\User')
            ->disableOriginalConstructor()
            ->getMock();

        $taskTitle = 'Test Task';
        $taskContent = 'This is a test task';

        $mockedTaskRepository= $this->createMock(TaskRepository::class);

        $taskService = new TaskService($mockedTaskRepository, $entityManager);

        $task = $taskService->createTask($taskTitle, $mockedAuthor, $taskContent);

        $this->assertInstanceOf(Task::class, $task);
        $this->assertEquals($taskTitle, $task->getTitle());
        $this->assertEquals($taskContent, $task->getContent());
        $this->assertEquals($mockedAuthor, $task->getAuthor());
    }

    public function testGetTasks()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $mockedTask1 = $this->getMockBuilder('App\Entity\Task')
            ->disableOriginalConstructor()
            ->getMock();

        $mockedTask2 = $this->getMockBuilder('App\Entity\Task')
            ->disableOriginalConstructor()
            ->getMock();

        $mockedTaskRepository = $this->getMockBuilder('App\Repository\TaskRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $mockedTaskRepository->expects($this->once())
            ->method('findAll')
            ->willReturn([$mockedTask1, $mockedTask2]);

        $taskService = new TaskService($mockedTaskRepository, $entityManager);

        $tasks = $taskService->getAllTasks();

        $this->assertEquals([$mockedTask1, $mockedTask2], $tasks);
    }
}
