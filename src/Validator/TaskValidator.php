<?php

namespace App\Validator;

use App\Entity\Task;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class TaskValidator
{
    public const TITLE_MIN_LENGTH = 4;
    public const TITLE_MAX_LENGTH = 34;
    public const CONTENT_MAX_LENGTH = 255;
    public const TITLE_REQUIRED = 'Title is required.';
    public const TITLE_TOO_SHORT = 'Title length is too short';
    public const TITLE_TOO_LONG = 'Title length is too Long';
    public const DUE_DATE_INVALID = 'Due date is invalid.';
    public const CONTENT_TOO_LONG = 'Content length is too Long';

    /**
     * @param Task $task
     * @return bool
     */
    public function validate(Task $task): bool
    {
        if (empty($task->getTitle())) {
            throw new InvalidParameterException(TaskValidator::TITLE_REQUIRED);
        }

        if (strlen($task->getTitle()) < TaskValidator::TITLE_MIN_LENGTH) {
            throw new InvalidParameterException(TaskValidator::TITLE_TOO_SHORT);
        }

        if (strlen($task->getTitle()) >= TaskValidator::TITLE_MAX_LENGTH) {
            throw new InvalidParameterException(TaskValidator::TITLE_TOO_LONG);
        }

        if ( !empty($task->getDueDate()) && $task->getDueDate() < new \DateTime('now')) {
            throw new InvalidParameterException(TaskValidator::DUE_DATE_INVALID);
        }

        if (strlen($task->getContent()) >= TaskValidator::CONTENT_MAX_LENGTH) {
            throw new InvalidParameterException(TaskValidator::CONTENT_TOO_LONG);
        }

        return true;
    }
}
