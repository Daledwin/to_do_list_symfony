<?php

namespace App\Validator;

use App\Entity\User;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class UserValidator
{
    public const FIRST_LAST_NAME_MIN_LENGTH = 2;
    public const EMAIL_MIN_LENGTH = 6;
    public const FIRSTNAME_REQUIRED = 'Firstname is required.';
    public const LASTNAME_REQUIRED = 'Lastname is required.';
    public const EMAIL_REQUIRED = 'Email is required.';
    public const PASSWORD_REQUIRED = 'Password is required.';
    public const FIRSTNAME_TOO_SHORT = 'Firstname length is too short.';
    public const LASTNAME_TOO_SHORT = 'Lastname length is too short.';
    public const EMAIL_TOO_SHORT = 'Email length is too short.';
    public const EMAIL_FORMAT_INVALID = 'The email format isn\'t wright .';

    /**
     * @param User $author
     * @return bool
     */
    public function validate(User $author): bool
    {
        if (empty($author->getFirstName())) {
            throw new InvalidParameterException(UserValidator::FIRSTNAME_REQUIRED);
        }

        if (strlen($author->getFirstName()) < UserValidator::FIRST_LAST_NAME_MIN_LENGTH) {
            throw new InvalidParameterException(UserValidator::FIRSTNAME_TOO_SHORT);
        }

        if (empty($author->getLastName())) {
            throw new InvalidParameterException(UserValidator::LASTNAME_REQUIRED);
        }

        if (strlen($author->getLastName()) < UserValidator::FIRST_LAST_NAME_MIN_LENGTH) {
            throw new InvalidParameterException(UserValidator::LASTNAME_TOO_SHORT);
        }

        if (empty($author->getEmail())) {
            throw new InvalidParameterException(UserValidator::EMAIL_REQUIRED);
        }

        if (strlen($author->getEmail()) < UserValidator::EMAIL_MIN_LENGTH) {
            throw new InvalidParameterException(UserValidator::EMAIL_TOO_SHORT);
        }

        if (!filter_var($author->getEmail(), FILTER_VALIDATE_EMAIL)) {
            throw new InvalidParameterException(UserValidator::EMAIL_FORMAT_INVALID);
        }

        if (empty($author->getPassword())) {
            throw new InvalidParameterException(UserValidator::PASSWORD_REQUIRED);
        }

        return true;
    }
}
