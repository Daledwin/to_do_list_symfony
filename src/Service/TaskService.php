<?php

namespace App\Service;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class TaskService
{
    private TaskRepository $taskRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(TaskRepository $taskRepository, EntityManagerInterface $entityManager)
    {
        $this->taskRepository = $taskRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return Task[]
     */
    public function getAllTasks(): array
    {
        return $this->taskRepository->findAll();
    }

    public function createTask( string $title, User $author, string $content, ?\DateTime $dueDate = null, bool $status = false): Task
    {
        $task = new Task($title, $author, $content, $dueDate, $status);
        $this->entityManager->persist($task);
        $this->entityManager->flush();
        return $task;
    }

    public function deleteTask(Task $task): void
    {
        $this->entityManager->remove($task);
        $this->entityManager->flush();
    }

    public function markTaskAsDone(Task $task): void
    {
        $task->setStatus(true);
        $this->entityManager->flush();
    }

    public function markTaskAsUndone(Task $task): void
    {
        $task->setStatus(false);
        $this->entityManager->flush();
    }
}
