<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserService
{
    private UserRepository $authorRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(UserRepository $authorRepository, EntityManagerInterface $entityManager)
    {
        $this->authorRepository = $authorRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @return User[]
     */
    public function getAllUsers(): array
    {
        return $this->authorRepository->findAll();
    }

    public function createUser(User $author): void
    {
        $this->entityManager->persist($author);
        $this->entityManager->flush();
    }

    public function deleteUser(User $author): void
    {
        $this->entityManager->remove($author);
        $this->entityManager->flush();
    }
}
